using System;
using UnityEngine;

public class CrashController : MonoBehaviour
{

    private Animator _animator;
    [SerializeField] private float moveSpeed;
    private static readonly int MoveAmount = Animator.StringToHash("MoveAmount");

    private void Awake()
    {
        _animator = GetComponent<Animator>();

        if (_animator == null)
        {
            Debug.LogError("No se pudo obtener el componente animator");
        }
    }

    private void Update()
    {
        var h = Input.GetAxis("Horizontal");
        var v = Input.GetAxis("Vertical");

        var moveDirection = new Vector3(h, 0, v);
        moveDirection.Normalize();
        Move(moveDirection);
        RotateTowards(moveDirection);
    }

    private void RotateTowards(Vector3 moveDir)
    {
        transform.forward = Vector3.Lerp(transform.forward, moveDir, Time.deltaTime);
    }

    private void Move(Vector3 moveDir)
    {
        _animator.SetFloat(MoveAmount, moveDir.magnitude);
        transform.position += moveDir * (Time.deltaTime * moveSpeed);
    }
}
