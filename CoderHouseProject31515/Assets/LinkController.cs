using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LinkController : MonoBehaviour
{

    private LinkMovementController _controller;

    private Camera _camera;

    private CubeBullet _cubeBullet;
    private void Awake()
    {
        _controller = GetComponent<LinkMovementController>();
        _camera = Camera.main;
        _cubeBullet = FindObjectOfType<CubeBullet>();
    }

    private Vector2 Inputs()
    {
        var inputX = Input.GetAxis("Horizontal");
        var inputY = Input.GetAxis("Vertical");
        return new Vector2(inputX, inputY);
    }

    private void Update()
    {
        var input = Inputs();
       _controller.RecieveInputs(input.x, input.y);
    }
}