﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class CharacterData
{
    public string characterName;
    public string description;
    public int age;
    public float height;
    public Color hairColor;
    public string gender;
}

public class CharacterDescription : MonoBehaviour
{
    [SerializeField] private List<CharacterData> charactersData;

    private Dictionary<string, CharacterData> _characterToDescription;


    [SerializeField] private string nameToCheck;


    private void Awake()
    {
        _characterToDescription = new Dictionary<string, CharacterData>();

        for (int i = 0; i < charactersData.Count; i++)
        {
            _characterToDescription.Add(charactersData[i].characterName, charactersData[i]);
        }
    }

    /*
    private void Update()
    {

    }*/

    private void PrintNameDictionary(string characterName)
    {
        if (_characterToDescription.ContainsKey(characterName))
        {
            Debug.Log($"{_characterToDescription[characterName].age}");
        }
    }

    public int GetCharacterAge(string characterName)
    {
        if (_characterToDescription.ContainsKey(characterName))
        {
            return _characterToDescription[characterName].age;
        }

        return -1;
    }

    private void PrintNameList(string characterName)
    {
        for (int i = 0; i < charactersData.Count; i++)
        {
            var character = charactersData[i];
            if (character.characterName == characterName)
            {
                Debug.Log($"Character description is : {character.description}");
            }
        }

        foreach (var character in charactersData)
        {
            if (character.characterName == characterName)
            {
                Debug.Log($"Character description : {character.description}");
            }
        }
    }

    private void SearchWithWhile(string characterName)
    {
        var i = 0;

        while (i < charactersData.Count)
        {
            if (charactersData[i].characterName == characterName)
            {
                Debug.Log($"Character description is {charactersData[i].description}");
            }

            i++;
        }
    }
}