using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Stalfos_Move : MonoBehaviour
{
    [SerializeField] private float moveSpeed;
    [SerializeField] private float minimumDistanceToWaypoint = 1;

    private Animator _animator;
    private bool _playerIsNear;

    [SerializeField] private Transform[] waypoints;

    private List<string> names;

    [SerializeField] private CharacterDescription _descriptor;

    [SerializeField] private string characterToAsk;
private Sprite _nextTetrisSprite;
    private Stack<int> _numberStack;
    private Queue<int> _numberQueue;

    private int _index = 0;
    private void Start()
    {
        _animator = GetComponent<Animator>();


        var ageObtained = _descriptor.GetCharacterAge(characterToAsk);

        if (ageObtained > 0)
        {
            Debug.Log($"{characterToAsk}'s age is {ageObtained}");
        }
        else
        {
            Debug.LogError($"No character {characterToAsk} found");
        }
    }

   
    private void Update()
    {
        /*
        if (_playerIsNear)
        {
            return;
        }*/

        Patrol();
    }

    private void Patrol()
    {
        //Patrol code 
        
        //Quien es B? Waypoint
        //Quien es A? Stalfos_Move

        var initialDistance = waypoints[_index].position - transform.position;

        var dir = initialDistance.normalized;

        transform.position += moveSpeed * Time.deltaTime * dir;

        var distanceToWaypoint = initialDistance.magnitude;
        // var dir = waypoints

        if (distanceToWaypoint < minimumDistanceToWaypoint)
        {
            CheckNextWaypoint();
        }
    }

    public void ButtonTest()
    {
        Debug.Log($"Button test");

        Time.timeScale = 0;

    }

    public void ButtonTestResume()
    {
        Time.timeScale = 1;
    }
    private void CheckNextWaypoint()
    {
        _index++;
        if (_index == waypoints.Length)
        {
            Debug.Log("Se resetea");
            _index = 0;
        }
    }
    /*
    private void PlayerIsNear(out bool playerIsNear)
    {
        
    }*/
}