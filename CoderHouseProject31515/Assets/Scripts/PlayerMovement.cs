using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum DialogueTypes
{
    Hi,
    Goodbye,
    Combat,
    Death
}

public enum Teams
{
    Red,
    Blue,
    Yellow,
    Green
}

public enum Movability
{
    Movable,
    RigidButChangeable,
    Unmovable
}

public class PlayerMovement : MonoBehaviour, IMoveable
{
    public float Speed => speed;
    public bool IsStopped { get; private set; }

    [SerializeField] private float speed;
    [SerializeField] private Vector3 direction;
    [SerializeField] private CubeBullet cubePrefab;
    [SerializeField] private Transform spawnLocation;

    [SerializeField] private string dialogueHi;
    [SerializeField] private string dialogueGoodbye;
    [SerializeField] private string dialogueCombat;
    [SerializeField] private string dialogueDeath;
    [SerializeField] private string noDialogueFound;


    [SerializeField] private Movability playerMovability;


    private void Update()
    {
        if (IsStopped) return;

        if (Input.GetKeyDown(KeyCode.J))
        {
            Shoot();
        }

        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            ShowDialogue(DialogueTypes.Goodbye);
        }
    }
    private void ShowDialogue(DialogueTypes dialogueToShow)
    {
        Debug.Log($"Case selected was {(int) dialogueToShow}");
        switch (dialogueToShow)
        {
            case DialogueTypes.Hi:
                Debug.Log(dialogueHi);
                break;
            case DialogueTypes.Goodbye:
                Debug.Log(dialogueGoodbye);
                break;
            case DialogueTypes.Combat:
                Debug.Log(dialogueCombat);
                break;
            case DialogueTypes.Death:
                Debug.Log(dialogueDeath);
                break;
            default:
                Debug.Log(noDialogueFound);
                break;
        }
    }

    private void Shoot()
    {
        Instantiate(cubePrefab, spawnLocation.position, spawnLocation.rotation);
    }

    public void Move(Vector3 dir)
    {
        transform.position += dir * (Speed * Time.deltaTime);
    }

    public void Stop()
    {
        IsStopped = true;
    }

    private void Start()
    {
        switch (playerMovability)
        {
            case Movability.Movable:
                enabled = true;
                break;
            case Movability.RigidButChangeable:
                enabled = false;
                break;
            case Movability.Unmovable:
                Destroy(this);
                break;
        }
    }
}