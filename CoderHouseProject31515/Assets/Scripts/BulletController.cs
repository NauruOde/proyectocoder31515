﻿using System;
using UnityEngine;

public class BulletController : MonoBehaviour
{

    [SerializeField] private float speed;
    [SerializeField] private float lifetime;
    private float _counter;

    private Vector3 _direction;

    private void Awake()
    {
        _counter = Time.time + lifetime;
    }

    private void Update()
    {
        transform.position += _direction * (speed * Time.deltaTime);
        
        if (_counter > Time.time) return;
        
        Destroy(gameObject);
    }

    public void SetDirection(Vector3 newDirection)
    {
        _direction = newDirection;
    }
}