﻿namespace Interface
{
    public interface IDamageable
    {
        void TakeDamage(float damage);
        float MaxHealth { get; }
        void GetHeal(float healAmount);
    }
}