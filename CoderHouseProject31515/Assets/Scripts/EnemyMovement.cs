using System;
using UnityEngine;

public class EnemyMovement : MonoBehaviour, IMoveable
{
    [SerializeField] private bool checkDie;

    [SerializeField] private float bulletsInMagazine;
    [SerializeField] private bool missionFinished;

    [SerializeField] private float distanceToReachMuseum;
    [SerializeField] private bool hasMuseumKey;
    [SerializeField] private Transform museumTransform;

    [SerializeField] private float counterToKill;
    private float _counterToKill;
    [SerializeField] private float health;

    [SerializeField] private int bulletsPerSecond;

    private float TimeBetweenBullets => 1f / bulletsPerSecond;

    private float _counter = 0;
    
    [SerializeField] private float numb;


    private void Start()
    {
        if (health != 0)
        {
            Debug.Log($"I'm not an undead");
        }
        else
        {
            Debug.Log($"I'm an undead");
        }
        
        
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (IsStopped) return;
        Move();

        CanEnterMuseum();
        */

        var inputX = Input.GetAxis("Horizontal");
        var inputY = Input.GetAxis("Vertical");


        var newDirection = new Vector3(inputX,0, inputY).normalized;

        Move(newDirection);
        //_counter += Time.deltaTime;
        
        if (Input.GetKey(KeyCode.P))
        {
            if (_counter < Time.time)
            {
                Shoot();
            }
        }
    }

    private void Shoot()
    {
        _counter = Time.time + TimeBetweenBullets;
        Debug.Log($"Shoot");
    }
    public void Move(Vector3 direction)
    {
        transform.position += Time.deltaTime * Speed * direction;
    }

    private void MuseumEnter()
    {
        if (CanEnterMuseum())
        {
            EnterMuseum();
        }

        else
        {
            Debug.Log($"I can't enter the museum");
        }
    }

    private bool CanEnterMuseum()
    {
        return CheckMuseumDistance() && HasMuseumKey() && !missionFinished;
    }

    private bool CheckMuseumDistance()
    {
        return Vector3.Distance(transform.position, museumTransform.position) < distanceToReachMuseum;
    }

    private bool HasMuseumKey()
    {
        return hasMuseumKey;
    }

    private void EnterMuseum()
    {
        Debug.Log($"I can enter the museum");
    }


    public float Speed => speed;
    public bool IsStopped { get; private set; }
    [SerializeField] private float speed;

    private Vector3 direction;

   

    public void Stop()
    {
        throw new NotImplementedException();
    }


    private void MoveToTheLeft()
    {
    }


    public void TakeDamage(int damage)
    {
        _health -= damage;

        if (_health > 0) return;

        _health = 0;
        Die();
    }

    private void Die()
    {
        Debug.Log($"The character has died");
        Destroy(gameObject);
    }

    private int _health;

    public int MaxHealth { get; }
}