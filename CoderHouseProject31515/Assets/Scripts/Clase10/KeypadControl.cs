using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KeypadControl : MonoBehaviour
{
    private int _index;
    private int[] _combination;
    [SerializeField] private int[] correctCombination;

    //   public event Action OnOpenDoor;

    [SerializeField] private UnityEvent onOpenDoor;

    private void Awake()
    {

        for (int i = 0; i < correctCombination.Length; i++)
        {
            if (correctCombination[i] >= 10 || correctCombination[i] < 1)
            {
                Debug.LogError("Combination is not properly set");
                Debug.Break();
                return;
            }
        }
        
        var allKeypadKeys = GetComponentsInChildren<KeypadKey>();
        for (int i = 0; i < allKeypadKeys.Length; i++)
        {
            allKeypadKeys[i].OnKeypadKeyPressed += OnKeypadKeyPressedHandler;
            allKeypadKeys[i].OnKeypadKeyPressed += (asdf) => ReceivedAKey(); 
        }

        _combination = new int[correctCombination.Length];
    }

    private void OnKeypadKeyPressedHandler(int keyPressed)
    {
        _combination[_index] = keyPressed;
        _index++;
        if (_index == correctCombination.Length)
        {
            _index = 0;

            if (IsCombinationCorrect())
            {
                onOpenDoor.Invoke();
            }
        }

        Debug.Log($"Key pressed {keyPressed}");
    }

    private void ReceivedAKey()
    {
        Debug.Log("Received a key");
    }
    
    
    private void OpenDoor()
    {
        Debug.Log("Open door");
    }

    private bool IsCombinationCorrect()
    {
        for (int i = 0; i < correctCombination.Length; i++)
        {
            if (_combination[i] != correctCombination[i])
            {
                return false;
            }
        }

        return true;
    }
}