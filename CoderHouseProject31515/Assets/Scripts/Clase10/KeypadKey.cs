using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeypadKey : MonoBehaviour
{
    [SerializeField] private int assignedKey;

    private KeypadControl _parentKeypad;

    public event Action<int> OnKeypadKeyPressed;


    public void SetParent(KeypadControl parentKeypad)
    {
        _parentKeypad = parentKeypad;
    }

    private void OnMouseDown()
    {
        OnKeypadKeyPressed?.Invoke(assignedKey);
    }
}