using System;
using System.Collections;
using System.Collections.Generic;
using After.Steering;
using After.Steering.Progress;
using UnityEngine;

public class PikminBase : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private OlimarMove olimar;

    private ISteeringBehaviourWIP _seek;
    private ISteeringBehaviourWIP _avoid;
    private ISteeringBehaviourWIP _pursuit;
    private ISteeringBehaviourWIP _flee;


    [SerializeField] private float predictionTime;

    private ISteeringBehaviourWIP _currentBehaviour;

    private float _timer = 30f;
    [SerializeField] private float startTime = 30f;

    private void Update()
    {
        var dir = _currentBehaviour.GetDir();
        transform.position += dir * (speed * Time.deltaTime);


        if (Input.GetKeyDown(KeyCode.J))
        {
            SwitchBehaviour(_seek);
        }

        if (Input.GetKeyDown(KeyCode.K))
        {
            SwitchBehaviour(_avoid);
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            SwitchBehaviour(_pursuit);
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            SwitchBehaviour(_flee);
        }
    }

    private void Start()
    {
        _seek = new SeekTarget(transform, olimar.transform);
        _avoid = new AvoidTarget(transform, olimar.transform);
        _pursuit = new PursuitTarget(transform, olimar.transform, predictionTime, olimar);
        _flee = new FleeTarget(transform, olimar.transform, predictionTime, olimar);
        _currentBehaviour = _seek;
    }

    private void SwitchBehaviour(ISteeringBehaviourWIP newBehaviour)
    {
        _currentBehaviour = newBehaviour;
    }
}