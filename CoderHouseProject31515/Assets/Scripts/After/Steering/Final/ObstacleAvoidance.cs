﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleAvoidance : ISteering
{
    private Transform _self;
    private Transform _target;
    private float _checkRadius;  
    private Collider[] obstaclesColliders;
    private LayerMask _obstaclesMask;
    private float _multiplier;
    public ObstacleAvoidance (Transform self,Transform target, float radius, int maxObjs, LayerMask obstaclesMask, float multiplier)
    {
        _self = self;
        _target = target;
        _checkRadius = radius;
        obstaclesColliders = new Collider[maxObjs];
        _obstaclesMask = obstaclesMask;
        _multiplier = multiplier;
    }
    public Vector3 GetDir()
    {
        Vector3 dir = (_target.position - _self.position).normalized;

        int countObjs = Physics.OverlapSphereNonAlloc(_self.position, _checkRadius, obstaclesColliders, _obstaclesMask);      

        Collider nearestObject = null;
        float distanceNearObj = 0;

        for (int i = 0; i < countObjs; i++)
        {
            var curr = obstaclesColliders[i];
            if (_self.position == curr.transform.position) continue;
            Vector3 closestPointToSelf = curr.ClosestPointOnBounds(_self.position);
            float distanceCurr = Vector3.Distance(_self.position, closestPointToSelf);

            if (nearestObject == null)
            {
                nearestObject = curr;
                distanceNearObj = distanceCurr;
            }
            else
            {               
                float distance = Vector3.Distance(_self.position, curr.transform.position);
                if (distanceNearObj > distance)
                {
                    nearestObject = curr;
                    distanceNearObj = distanceCurr;
                }
            }
        }

        if (nearestObject != null)
        {
            var posObj = nearestObject.transform.position;
            Vector3 dirObstacleToSelf = (_self.position - posObj);          
            dirObstacleToSelf= dirObstacleToSelf.normalized * ((_checkRadius - distanceNearObj) / _checkRadius) * _multiplier;
            dir += dirObstacleToSelf;
            dir = dir.normalized;
        }

        return dir;
    }
}
