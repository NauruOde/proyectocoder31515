﻿using UnityEngine;

namespace After.Steering
{
    public class PursuitEscape : MonoBehaviour
    {
        [SerializeField] private Transform olimar;
        [SerializeField] private float speed;
        [SerializeField] private bool follow;

        private void Update()
        {
            if (follow)
            {
                MoveTowards();
            }
            else
            {
                MoveAway();
            }
        }

        private void MoveTowards()
        {
            var direction = olimar.position - transform.position;
            direction.Normalize();

            transform.position += direction * (speed * Time.deltaTime);
        }

        private void MoveAway()
        {
            var direction = transform.position - olimar.position ;
            direction.Normalize();
            transform.position += direction * (speed * Time.deltaTime);
        }
    }
}