﻿using UnityEngine;

namespace After.Steering.Progress
{
    public class AvoidTarget: ISteeringBehaviourWIP
    {
        private Transform _self;
        private Transform _target;


        public AvoidTarget(Transform self, Transform target)
        {
            _self = self;
            _target = target;
        }
        public Vector3 GetDir()
        {
            var direction = _self.position - _target.position;
            direction.Normalize();

            return direction;
        }

        public void SetTarget(Transform newTarget)
        {
            _target = newTarget;
        }

        public void SetSelf(Transform newSelf)
        {
            _self = newSelf;
        }
    }
}