﻿using UnityEngine;

namespace After.Steering.Progress
{
    public interface ISteeringBehaviourWIP
    {
        Vector3 GetDir();
        void SetTarget(Transform newTarget);
        void SetSelf(Transform newSelf);
    }
}