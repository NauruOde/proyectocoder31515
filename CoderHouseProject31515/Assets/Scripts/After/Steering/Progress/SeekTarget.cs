﻿using UnityEngine;

namespace After.Steering.Progress
{
    public class SeekTarget: ISteeringBehaviourWIP
    {

        private Transform _self;
        private Transform _target;

        public SeekTarget(Transform self, Transform target)
        {
            _self = self;
            _target = target;
        }
        
        
        public Vector3 GetDir()
        {
            var direction = _target.position - _self.position;
            direction.Normalize();
            return direction;
        }

        public void SetTarget(Transform newTarget)
        {
            _target = newTarget;
        }

        public void SetSelf(Transform newSelf)
        {
            _self = newSelf;
        }
    }
}