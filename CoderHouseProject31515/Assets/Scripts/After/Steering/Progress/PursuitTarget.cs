﻿using UnityEngine;

namespace After.Steering.Progress
{
    public class PursuitTarget: ISteeringBehaviourWIP
    {
        private Transform _self;
        private Transform _target;
        private IVelocity _velocityObject;
        private float _predictionTime;

        public PursuitTarget(Transform self, Transform target, float predictionTime, IVelocity velocityObject)
        {
            _self = self;
            _target = target;
            _predictionTime = predictionTime;
            _velocityObject = velocityObject;
        }
        
        
        public Vector3 GetDir()
        {
            var directionMultiplier = (_velocityObject.Vel * _predictionTime);
            var distance = Vector3.Distance(_target.position, _self.position);

            if (directionMultiplier >= distance)
            {           
                directionMultiplier = distance / 2;
            }
            var finitPos = _target.position + _target.forward * directionMultiplier;
            var dir = (finitPos - _self.position).normalized;
            return dir;
        }

        public void SetTarget(Transform newTarget)
        {
            _target = newTarget;
        }

        public void SetSelf(Transform newSelf)
        {
            _self = newSelf;
        }
    }
}