﻿namespace After.Steering.Progress
{
    public interface IVelocity
    {
        float Vel { get; }
    }
}