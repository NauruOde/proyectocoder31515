﻿using System;
using After.Steering.Progress;
using UnityEngine;

namespace After.Steering
{
    public class OlimarMove : MonoBehaviour, IVelocity
    {
        [SerializeField] private float speed;
        [SerializeField] private float turnRate;


        private void Update()
        {
            Move();
        }

        private void Move()
        {
            var h = Input.GetAxis("Horizontal");
            var v = Input.GetAxis("Vertical");
            transform.position += transform.forward * (v * speed * Time.deltaTime);
            Vel = v * speed;
            transform.Rotate(Vector3.up * (turnRate * Time.deltaTime * h), Space.Self);
        }

        private float _vel;

        public float Vel
        {
            get { return _vel; }

            private set { _vel = value; }
        }
    }
}