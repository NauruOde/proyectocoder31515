using UnityEngine;

public class LinkAwakeningController : MonoBehaviour
{
    private Vector3 _movementDir;
    private Animator _animator;
    [SerializeField] private float movementSpeed;
    [SerializeField] private float rotationSpeed;
    private static readonly int MovementSpeed = Animator.StringToHash("MovementSpeed");


    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    private void Update()
    {
        _movementDir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        Move();
        Rotate();
    }

    private void Move()
    {
        _movementDir.Normalize();

        _animator.SetFloat(MovementSpeed, _movementDir.magnitude);

        if (_movementDir == Vector3.zero)
            return;
        transform.position += transform.forward * (_movementDir.z * (Time.deltaTime * movementSpeed));
    }

    private void Rotate()
    {
        if (_movementDir == Vector3.zero)
        {
            return;
        }

        transform.Rotate(Vector3.up * (_movementDir.x * rotationSpeed * Time.deltaTime), Space.Self);
    }
}