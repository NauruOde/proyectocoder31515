using System;
using System.Collections;
using System.Collections.Generic;
using After.TDAyFisicas.Interface;
using UnityEngine;
using UnityEngine.Networking;

public class LinkInteractor : MonoBehaviour
{
    [SerializeField] private Transform raycastPosition;
    [SerializeField] private float raycastLength;
    [SerializeField] private LayerMask layerToInteractWith;


    [SerializeField] private float interactRadius;

    private Collider[] _interactablesFound = new Collider[1];

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            TryInteract();
        }
    }

    private void OpenUserProvidedURL(string url)
    {
        var results = Uri.TryCreate(url, UriKind.Absolute, out var uriResult);

        if (results && uriResult.Scheme == Uri.UriSchemeHttp)
        {
            Application.OpenURL(url);
        }
        
       
    }
    
    
    private void TryInteract()
    {
        //Con Raycast

        /*
        RaycastHit hit;

        var hitSomething = Physics.Raycast(raycastPosition.position, raycastPosition.forward, out hit, raycastLength,
            layerToInteractWith);

        if (!hitSomething)
        {
            return;
        }
        
        if (hit.collider.gameObject.TryGetComponent(out IInteractable interactable))
        {
            interactable.OnInteract();
        }*/

        //Con Overlap Sphere

        /*
        var overlappingInteractables =
            Physics.OverlapSphere(raycastPosition.position, interactRadius, layerToInteractWith);

        if (overlappingInteractables.Length == 0)
        {
            return;
        }

        if (overlappingInteractables[0].gameObject.TryGetComponent(out IInteractable interactable))
        {
            interactable.OnInteract();
        }*/
        
        //Con overlapSphereNonAlloc

        var overlappingInteractables = Physics.OverlapSphereNonAlloc(raycastPosition.position, interactRadius,
            _interactablesFound, layerToInteractWith);
        
        if (overlappingInteractables == 0)
            return;
        
        if (_interactablesFound[0].gameObject.TryGetComponent(out IInteractable interactable))
        {
            interactable.OnInteract();
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(raycastPosition.position, interactRadius);
    }
}