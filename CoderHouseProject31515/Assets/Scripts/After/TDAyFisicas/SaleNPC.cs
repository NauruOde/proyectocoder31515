using System.Collections;
using System.Collections.Generic;
using After.TDAyFisicas.Interface;
using UnityEngine;

public class SaleNPC : MonoBehaviour, IInteractable
{
    [TextArea(3,10)][SerializeField] private string dialogue;
    
    public void OnInteract()
    {
        Debug.Log(dialogue);
    }
}
