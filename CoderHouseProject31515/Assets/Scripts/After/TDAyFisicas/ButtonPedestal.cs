using System;
using After.TDAyFisicas.Interface;
using UnityEngine;

public class ButtonPedestal : MonoBehaviour,IInteractable
{

    private Renderer _renderer;
    [SerializeField] private Color oldColor, newColor;

    private bool _isOldColor;

    private void Awake()
    {
        _renderer = GetComponentInChildren<Renderer>();
        _isOldColor = true;
    }

    public void OnInteract()
    {
        _isOldColor = !_isOldColor;
        _renderer.material.SetColor("_Color", _isOldColor ? oldColor : newColor);
    }
}
