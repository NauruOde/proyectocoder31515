using System.Collections;
using System.Collections.Generic;
using After.TDAyFisicas.Interface;
using UnityEngine;

public class OwlStatue : MonoBehaviour, IInteractable
{

    [SerializeField] private string itemToCheckID;
    [TextArea(3,10)][SerializeField] private string dialogue;

    [SerializeField] private Transform itemNewPosition;

    private bool _hasBeak;
    


    public void OnInteract()
    {
        if (_hasBeak || !Inventory.Instance.HasItem(itemToCheckID))
        {
            Debug.LogError("Player doesn't have the beak item");
            return;
        }
        var beakItem = Inventory.Instance.GetItemFromPool(itemToCheckID);

        beakItem.gameObject.SetActive(true);
        _hasBeak = true;
        beakItem.transform.parent = itemNewPosition;
        beakItem.transform.localPosition = Vector3.zero;
        beakItem.transform.localRotation = Quaternion.identity;

        Debug.Log(dialogue);
    }
}
