using UnityEngine;

namespace After.TDAyFisicas.Interface
{
    public interface IInteractable
    {
        void OnInteract();
    }
}
