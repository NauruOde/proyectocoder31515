using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public static Inventory Instance;

    private Dictionary<string, PickupableItem> _itemDictionary;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;

        _itemDictionary = new Dictionary<string, PickupableItem>();
    }

    public void GetItem(PickupableItem itemToPickup)
    {
        _itemDictionary.Add(itemToPickup.id, itemToPickup);
        itemToPickup.gameObject.SetActive(false);
    }
    public bool HasItem(PickupableItem itemToCheck)
    {
        return _itemDictionary.ContainsKey(itemToCheck.id);
    }
    
    public bool HasItem(string itemID)
    {
        return _itemDictionary.ContainsKey(itemID);
    }

    public bool HasItems(List<PickupableItem> itemsToCheck)
    {
        for (int i = 0; i < itemsToCheck.Count; i++)
        {
            var itemToCheck = itemsToCheck[i];
            if (!_itemDictionary.ContainsKey(itemToCheck.id))
            {
                return false;
            }
        }

        return true;
    }

    public PickupableItem GetItemFromPool(string idToCheck)
    {
        if (_itemDictionary.ContainsKey(idToCheck))
        {
            var itemToReturn = _itemDictionary[idToCheck];
            _itemDictionary.Remove(idToCheck);
            return itemToReturn;
        }

        return null;
    }
}