﻿public interface ILoadable
{
    void OnLoad(PlayerSaveData data);
}