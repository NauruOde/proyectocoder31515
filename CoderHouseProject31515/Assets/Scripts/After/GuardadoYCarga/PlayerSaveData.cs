﻿public class PlayerSaveData
{
    public WeaponData weapon;
    public string playerName;
    public float playerHealth;
}