using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using File = System.IO.File;

[Serializable]
public class SavedPlayerData
{
    public float playerHealth;
    public Vector3 playerPosition;
    public string playerName;
    public bool hasWeapon;
    public string weaponID;

    
    public SavedPlayerData(float playerHealth, Vector3 playerPosition, string playerName, bool hasWeapon, string weaponID)
    {
        this.playerHealth = playerHealth;
        this.playerPosition = playerPosition;
        this.playerName = playerName;
        this.hasWeapon = hasWeapon;
        this.weaponID = weaponID;
    }
}

public class PlayerLoadController : MonoBehaviour, ILoadable
{
    [SerializeField] private float moveSpeed;
    [SerializeField] private Transform weaponPositionParent;
    private Animator _animator;
    private WeaponsController _weaponsController;
    private bool _canMove;

    [SerializeField] private float health;
    [SerializeField] private string playerName;
    private static readonly int WalkSpeed = Animator.StringToHash("WalkSpeed");

    private const string SAVE_PLAYER_POSITION_X = "PlayerPositionX";
    private const string SAVE_PLAYER_POSITION_Y = "PlayerPositionY";
    private const string SAVE_PLAYER_POSITION_Z = "PlayerPositionZ";
    private const string SAVE_PLAYER_HEALTH = "PlayerHealth";
    private const string SAVE_PLAYER_NAME = "PlayerName";
    private const string SAVE_PLAYER_HAS_WEAPON = "PlayerHasWeapon";
    private const string SAVE_PLAYER_WEAPON_ID = "WeaponSpriteID";

    private Dictionary<string, Sprite> _idToSprite;

    private void Awake()
    {
        _weaponsController = new WeaponsController(weaponPositionParent);
        _animator = GetComponent<Animator>();
        _canMove = true;

        _weaponsController.SetHasWeapon(true);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Weapon weapon))
        {
            _weaponsController.GetWeapon(weapon.WeaponData);
            weapon.transform.parent = weaponPositionParent;
            weapon.transform.position = Vector3.zero;
            weapon.GetComponent<Collider>().enabled = false;
        }
    }

    private void Update()
    {
        Move(new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")));

        if (Input.GetKeyDown(KeyCode.K))
        {
            Attack();
        }

        if (Input.GetKeyDown(KeyCode.J))
        {
            Save();
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            Load();
        }
    }

    private void Load()
    {
        /*
        var loadedPlayerName = PlayerPrefs.GetString(SAVE_PLAYER_NAME, "none");
        var loadedPlayerHealth = PlayerPrefs.GetFloat(SAVE_PLAYER_HEALTH, 0);
        var loadedPositionX = PlayerPrefs.GetFloat(SAVE_PLAYER_POSITION_X, 0);
        var loadedPositionY = PlayerPrefs.GetFloat(SAVE_PLAYER_POSITION_Y, 0);
        var loadedPositionZ = PlayerPrefs.GetFloat(SAVE_PLAYER_POSITION_Z, 0);
        var loadedPosition = new Vector3(loadedPositionX, loadedPositionY, loadedPositionZ);
        var loadedHasWeapon = PlayerPrefs.GetInt(SAVE_PLAYER_HAS_WEAPON, 0) != 0;
        var loadedWeaponID = PlayerPrefs.GetString(SAVE_PLAYER_WEAPON_ID, "none");

        var loadedWeaponSprite = _idToSprite[loadedWeaponID];
        
        playerName = loadedPlayerName;
        health = loadedPlayerHealth;
        transform.position = loadedPosition;
        Debug.Log($"Has weapon {loadedHasWeapon}");
        _weaponsController.SetHasWeapon(loadedHasWeapon);
        */
        var jsonObject = File.ReadAllText(Application.dataPath + "/SaveData.json");
        var savedData = JsonUtility.FromJson<SavedPlayerData>(jsonObject);

        playerName = savedData.playerName;
        health = savedData.playerHealth;
        transform.position = savedData.playerPosition;
    }

    private void Save()
    {
        /*
        PlayerPrefs.SetFloat(SAVE_PLAYER_HEALTH, health);
        var position = transform.position;
        PlayerPrefs.SetFloat(SAVE_PLAYER_POSITION_X, position.x);
        PlayerPrefs.SetFloat(SAVE_PLAYER_POSITION_Y, position.y);
        PlayerPrefs.SetFloat(SAVE_PLAYER_POSITION_Z, position.z);
        PlayerPrefs.SetString(SAVE_PLAYER_NAME, playerName);
        PlayerPrefs.SetInt(SAVE_PLAYER_HAS_WEAPON, _weaponsController.HasWeapon? 1: 0);
        PlayerPrefs.SetString(SAVE_PLAYER_WEAPON_ID, "smallWeapon");
        PlayerPrefs.Save();
        */

        var saveData =
            new SavedPlayerData(health, transform.position, playerName, _weaponsController.HasWeapon, "none");

        var stringjson =JsonUtility.ToJson(saveData);
        File.WriteAllText(Application.dataPath + "/SaveData.json", stringjson);
        print("Saving");
        
        
    }


    private void Attack()
    {
        if (!_weaponsController.HasWeapon)
            return;

        _canMove = false;
        _animator.Play(_weaponsController.CurrentWeapon.heavyWeapon ? "HeavyAttack" : "LightAttack");
    }

    private void Move(Vector3 moveDir)
    {
        if (!_canMove)
        {
            _animator.SetFloat(WalkSpeed, 0);
            return;
        }

        _animator.SetFloat(WalkSpeed, moveDir.magnitude);
        var transform1 = transform;
        transform1.position +=
            (transform1.forward * moveDir.z + transform1.right * moveDir.x) * (moveSpeed * Time.deltaTime);
    }

    public void OnLoad(PlayerSaveData saveData)
    {
        health = saveData.playerHealth;
        playerName = saveData.playerName;
        if (saveData.weapon != null)
        {
            _weaponsController.GetWeapon(saveData.weapon);
        }
    }
}