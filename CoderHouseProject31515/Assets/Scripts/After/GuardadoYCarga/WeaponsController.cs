﻿using UnityEngine;

public class WeaponsController
{
    public WeaponData CurrentWeapon { get; private set; }
    private Transform _weaponPositionParent;

    public bool HasWeapon { get; private set; }
    

    public WeaponsController(Transform weaponPositionParent)
    {
        _weaponPositionParent = weaponPositionParent;
    }
    
    public void GetWeapon(WeaponData weapon)
    {
        CurrentWeapon = weapon;
        HasWeapon = true;
    }

    public void SetHasWeapon(bool newValue)
    {
        HasWeapon = newValue;
    }
}