﻿using UnityEngine;


[CreateAssetMenu(fileName = "NewWeaponData", menuName = "ScriptableObjects/WeaponData", order = 0)]
public class WeaponData : ScriptableObject
{

    public bool heavyWeapon;
    public float attackDamage;

}