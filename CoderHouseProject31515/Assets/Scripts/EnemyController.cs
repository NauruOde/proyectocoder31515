using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField] private bool isHuman;
    [SerializeField] private int age;
    [SerializeField] private int age2;
    [SerializeField] private float speed;
    [SerializeField] private double airPressure;
    [SerializeField] private string name;
    [SerializeField] private Vector3 movementAmount;
    [SerializeField] private int ageLimit;

    public float Speed => speed;
    
    public float GetSpeed()
    {
        return speed;
    }

}