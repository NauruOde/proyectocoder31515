using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private bool isHuman;
    [SerializeField] private int age;
    [SerializeField] private int age2;
    [SerializeField] private float speed;
    [SerializeField] private double airPressure;
    [SerializeField] private string name;
    [SerializeField] private Vector3 movementAmount;
    [SerializeField] private int ageLimit;
    private bool _isDead;

    private int _health;


    public void SetHealth(int newHealth)
    {
        _health = newHealth;
        Debug.Log($"Player is dead {_health <= 0}");
    }

    public int GetHealth()
    {
        return _health;
    }

    [SerializeField] private EnemyController enemyController;

    [SerializeField] private Camera myCamera;

    private void Awake()
    {
        Initialize();
    }

    private void Initialize()
    {
        Debug.Log(enemyController.name);
    }

    public int Sum(int number1, int number2)
    {
        return number1 + number2;
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(myCamera.transform.position);
    }
}