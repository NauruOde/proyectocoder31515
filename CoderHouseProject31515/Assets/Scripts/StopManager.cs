using System;
using System.Collections;
using System.Collections.Generic;
using Interface;
using UnityEngine;

public class StopManager : MonoBehaviour
{

    [SerializeField] private GameObject obj;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            obj.GetComponent<IMoveable>()?.Stop();
            obj.GetComponent<IDamageable>().TakeDamage(10);
        }
        
        
    }
}
