using UnityEngine;

public class ItemsContainer : MonoBehaviour
{

    [SerializeField] private FieldItem[] itemsToSpawn;


    private void Awake()
    {
        foreach (var currentItem in itemsToSpawn)
        {
            //Metodo 1 de downcasting
            /*
            var boxItem = currentItem as RandomBoxItem;

            if (boxItem != null)
            {
                boxItem.CheckBoxItem();
            }*/
            
            //Metodo 2 de DownCasting

            /*
            var boxItem = (RandomBoxItem) currentItem;

            if (boxItem != null)
            {
                boxItem.CheckBoxItem();
            }*/
        }
    }
}
