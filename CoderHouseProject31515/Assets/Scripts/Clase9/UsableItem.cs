﻿using System;
using UnityEngine;

public class UsableItem : FieldItem
{
    
    
    protected override void UseItem()
    {
        AudioSource.PlayClipAtPoint(itemData.ItemPickupSound, transform.position);
        itemData.UIEvent.Raise(new UiEventParam()
        {
            Command = UICommands.SetItemSprite,
            ItemSprite = null
        });
        Destroy(gameObject);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.J) && _pickedUp)
        {
            UseItem();
        }
    }
}