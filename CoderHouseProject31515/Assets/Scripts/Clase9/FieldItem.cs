using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FieldItem : MonoBehaviour
{

    [SerializeField] private Renderer _renderer;
    [SerializeField] protected ItemData itemData;

    protected int Points => itemData.Points;
    protected string ItemName => itemData.ItemName;
    protected bool _pickedUp;


    protected virtual void Pickup()
    { 
        var coll = GetComponent<Collider>().enabled = false;
        _renderer.enabled = false;
        //InGameCanvas.Instance.SetItemSprite(itemData.ItemSprite);
        itemData.UIEvent.Raise(new UiEventParam()
        {
            Command = UICommands.SetItemSprite,
            ItemSprite = itemData.ItemSprite
        });
    }

    protected abstract void UseItem();

    public virtual string GetItemName()
    {
        return this.itemData.ItemName;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!_pickedUp)
        {
            if (other.TryGetComponent(out CarController player))
            {
                player.ItemGet(itemData);
                Debug.Log(other.name);
                _pickedUp = true;
                Pickup();
            }
        }
    }
}