using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    [SerializeField] private float moveSpeed;
    [SerializeField] private float rotationSpeed;
    private Vector3 _dir;

    private int _coinCounter;

    [SerializeField] private UIEvent uiEvent;
    private FieldItem _item;

    private void Update()
    {
        Move();
    }


    private void Move()
    {
        _dir.x = Input.GetAxis("Horizontal");
        _dir.z = Input.GetAxis("Vertical");
        Rotate();
        transform.position += transform.forward * (_dir.z * moveSpeed * Time.deltaTime);
    }

    private void Rotate()
    {
        transform.Rotate(Vector3.up * (rotationSpeed * Time.deltaTime * _dir.x), Space.Self);
    }

    private IEnumerator MoveFaster()
    {
        moveSpeed *= 2;

        yield return new WaitForSeconds(4);

        moveSpeed *= .5f;
    }

    public void ItemGet(ItemData item)
    {
        switch (item.ID)
        {
            case "Coin":
                _coinCounter += item.Points;
                uiEvent.Raise(new UiEventParam()
                {
                    Command = UICommands.SetCoin,
                    CoinsAmount = _coinCounter
                });
                break;
            case "Mushroom":
                StartCoroutine(MoveFaster());
                break;
        }
    }
}