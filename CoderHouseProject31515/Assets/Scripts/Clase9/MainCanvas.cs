using System;
using System.Threading;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainCanvas : MonoBehaviour
{
    private MainMenuItem _currentMenu;
    [SerializeField] private MainMenuItem defaultMenu;

    [SerializeField] private string sceneToLoad;


    private void Awake()
    {
        var allMenus = GetComponentsInChildren<MainMenuItem>();
        foreach (var menuItem in allMenus)
        {
            menuItem.CloseMenu();
        }
    }

    public void PlayGame()
    {
        SceneManager.LoadScene(sceneToLoad);
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        EditorApplication.ExitPlaymode();
#else
        Application.Quit();
#endif
    }

    private void Start()
    {
        _currentMenu = defaultMenu;
        if (_currentMenu != null)
        {
            _currentMenu.OpenMenu();
        }
    }

    public void ChangeMenu(MainMenuItem newMenu)
    {
        if (_currentMenu != null)
        {
            _currentMenu.CloseMenu();
        }

        _currentMenu = newMenu;
        _currentMenu.OpenMenu();
    }

    public void SetUsername(string newUsername)
    {
        SettingsManager.Instance.SetUsername(newUsername);
    }

    public void SetMusicVolume(float newVolume)
    {
        SettingsManager.Instance.SetMusicVolume(newVolume);
    }

    public void SetRaytracing(bool newValue)
    {
        SettingsManager.Instance.SetRaytracing(newValue);
    }
}

#if UNITY_EDITOR

[CustomEditor(typeof(MainCanvas))]
internal class MainCanvasEditor : Editor
{
    //
}
#endif