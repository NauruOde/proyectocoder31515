﻿using System;
using UnityEngine;
using UnityEngine.Events;



public enum UICommands
{
    SetCoin,
    SetItemSprite
}
    
[System.Serializable]
public struct UiEventParam
{
    public UICommands Command;
    public int CoinsAmount;
    public Sprite ItemSprite;
}

public class UIEventListener : MonoBehaviour
{
    [SerializeField] private UIEvent eventToListen;
    [SerializeField] private UnityEvent<UiEventParam> OnTrigger;


    private void OnEnable()
    {
        eventToListen.RegisterListener(this);
    }

    private void OnDisable()
    {
        eventToListen.UnregisterListener(this);
    }

    public void OnEventRaised(UiEventParam param)
    {
        OnTrigger.Invoke(param);
    }
    
    
}