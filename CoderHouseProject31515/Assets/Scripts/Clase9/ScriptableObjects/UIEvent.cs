﻿using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "UI event", menuName = "ScriptableObjects/Events", order = 0)]
public class UIEvent : ScriptableObject
{
    private List<UIEventListener> _listeners = new List<UIEventListener>();

    public void Raise(UiEventParam p)
    {
        for (int i = _listeners.Count - 1; i >= 0; i--)
        {
            _listeners[i].OnEventRaised(p);
        }
    }

    public void RegisterListener(UIEventListener listener)
    {
        _listeners.Add(listener);
    }

    public void UnregisterListener(UIEventListener listener)
    {
        _listeners.Remove(listener);
    }
}