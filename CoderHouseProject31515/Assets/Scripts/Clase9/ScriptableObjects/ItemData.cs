using UnityEngine;

[CreateAssetMenu (menuName = "ScriptableObjects/Item/ItemData", fileName = "NewItemData", order = 0)]
public class ItemData: ScriptableObject
{
   public string ID;
   public string ItemName;
   public Sprite ItemSprite;
   public int Points;
   public AudioClip ItemPickupSound;
   public UIEvent UIEvent;
}
