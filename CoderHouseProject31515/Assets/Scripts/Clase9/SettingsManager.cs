using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsManager : MonoBehaviour
{

    public static SettingsManager Instance;

    public bool RaytracingOn { get; private set; }
    public float MusicVolume { get; private set; }
    public string UserName { get; private set; }
    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;

        DontDestroyOnLoad(gameObject);
    }


    public void SetRaytracing(bool isOn)
    {
        RaytracingOn = isOn;
    }

    public void SetMusicVolume(float musicVolume)
    {
        MusicVolume = musicVolume;
    }

    public void SetUsername(string username)
    {
        UserName = username;
    }
}
