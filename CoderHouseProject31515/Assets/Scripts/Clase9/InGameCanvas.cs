using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InGameCanvas : MonoBehaviour
{
    [SerializeField] private Image itemImage;
    [SerializeField] private TextMeshProUGUI coinAmount;


    private void Awake()
    {
        SetItemSprite(null);
    }

    public void SetItemSprite(Sprite itemSprite)
    {
        itemImage.enabled = itemSprite != null;
        itemImage.sprite = itemSprite;
    }

    public void UIEvent(UiEventParam p)
    {
        switch (p.Command)
        {
            case UICommands.SetCoin:
                SetCoinAmount(p.CoinsAmount);
                break;
            case UICommands.SetItemSprite:
                SetItemSprite(p.ItemSprite);
                break;
        }
    }

    public void SetCoinAmount(int newCoinAmount)
    {
        coinAmount.text = newCoinAmount.ToString();
    }
}