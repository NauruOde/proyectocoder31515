using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxFinder : MonoBehaviour
{
    private void Awake()
    {
        var rbItem = FindObjectsOfType<RandomBoxItem>();
        
        Debug.Log($"RB item {rbItem.Length}");

        var fieldItems = FindObjectsOfType<FieldItem>();
        
        Debug.Log($"Field items {fieldItems.Length}");

        var usableItem = FindObjectsOfType<UsableItem>();
        Debug.Log($"Usable items {usableItem.Length}");
    }
}
