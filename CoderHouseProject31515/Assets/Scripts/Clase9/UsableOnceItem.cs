﻿using UnityEngine;
using UnityEngine.Events;


public class UsableOnceItem : UsableItem
{
   protected override void Pickup()
   {
      base.Pickup();
      UseItem();
   }

   protected override void UseItem()
   {
      base.UseItem();
      AudioSource.PlayClipAtPoint(itemData.ItemPickupSound, transform.position);
      itemData.UIEvent.Raise(new UiEventParam()
      {
         Command = UICommands.SetItemSprite,
         ItemSprite = null
      });
      Destroy(gameObject);
   }
}