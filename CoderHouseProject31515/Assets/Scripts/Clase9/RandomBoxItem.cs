using System;
using System.Collections;
using System.Collections.Generic;
using After.TDAyFisicas.Interface;
using UnityEngine;
using Random = UnityEngine.Random;

public class RandomBoxItem : FieldItem
{

    [SerializeField] private FieldItem[] itemsToProduce;
    protected override void Pickup()
    {
        base.Pickup();
        Debug.Log("Picking random item");
        UseItem();
    }

    protected override void UseItem()
    {
        var itemNumber = Random.Range(0, itemsToProduce.Length);
        Instantiate(itemsToProduce[itemNumber], transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    
}