using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CanvasName : MonoBehaviour
{
    [SerializeField] private Transform _cameraPosition;
    [SerializeField] private Transform _playerTransform;
    [SerializeField] private Transform canvasTransform;


    private void LateUpdate()
    {
        if (_cameraPosition == null)
            return;

        var dir = canvasTransform.position - _playerTransform.position + _cameraPosition.forward;
        dir.y = 0;
        canvasTransform.rotation = Quaternion.LookRotation(dir);
    }
}