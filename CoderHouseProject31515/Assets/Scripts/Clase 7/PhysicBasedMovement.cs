using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicBasedMovement : MonoBehaviour
{
    private Rigidbody _rigidbody;

    [SerializeField] private Vector3 forceDirection;
    [SerializeField] private float forceIntensity;

    [SerializeField] private ForceMode forceModeToUse;
    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            ApplyForces();
        }
    }

    private void ApplyForces()
    {
        forceDirection.Normalize();
        _rigidbody.AddForce(forceDirection * forceIntensity, forceModeToUse);
    }
}
