using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floating : MonoBehaviour
{
    [SerializeField] private float forceToApply;
    [SerializeField] private float timeToApply;
    [SerializeField] private Rigidbody _rb;
    private float _counter;
    private bool _floatObject;

    private void Update()
    {
        if (_counter < Time.time)
        {
            _counter = Time.time + timeToApply;
            _floatObject = !_floatObject;
        }
    }

    private void FixedUpdate()
    {
        _rb.AddForce(Vector3.up * (Time.fixedDeltaTime * (forceToApply * (_floatObject? 1: -1))), ForceMode.Acceleration);
    }
}