using System;
using System.Collections;
using System.Collections.Generic;
using Interface;
using UnityEngine;

public class FPSGun : MonoBehaviour
{
    [SerializeField] private Transform _camera;
    [SerializeField] private float range;
    [SerializeField] private LayerMask objectsToCollideWith;
    [SerializeField] private float damage;

    [SerializeField] private ParticleSystem shootingParticles;
    private void Update()
    {

    }

    private void Shoot()
    {
        Debug.Log("Shooting");

        RaycastHit hitData;

        if (Physics.Raycast(_camera.position, _camera.forward, out hitData, range, objectsToCollideWith))
        {
            var ps = Instantiate(shootingParticles, hitData.point, Quaternion.Euler(hitData.normal));
            Destroy(ps, 4f);            
            if (hitData.collider.TryGetComponent<IDamageable>(out var damageable))
            {
                damageable.TakeDamage(damage);
            }
        }
    }
}
