using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonWithBullet : MonoBehaviour
{
    [SerializeField] private Transform shootingPoint;

    [SerializeField] private GameObject bullet;
    [SerializeField] private float shootingForce;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        var bulletToShoot = Instantiate(bullet, shootingPoint.position, shootingPoint.rotation);

        var bulletRB = bulletToShoot.GetComponent<Rigidbody>();

        if (bulletRB != null)
        {
            bulletRB.AddForce(shootingPoint.rotation.eulerAngles * shootingForce, ForceMode.Impulse);
        }

        Destroy(bulletToShoot, 5f);
    }
}