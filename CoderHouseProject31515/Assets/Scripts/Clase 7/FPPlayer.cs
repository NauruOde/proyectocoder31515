using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPPlayer : MonoBehaviour
{
    [SerializeField] private float walkSpeed;
    [SerializeField] private float runSpeed;
    private float _currentMoveSpeed;
    [SerializeField] private float rotationSpeed;

    private Vector3 _direction = new Vector3(0, 0, 0);


    private void Awake()
    {
        _currentMoveSpeed = walkSpeed;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            RunToggle(true);
        }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            RunToggle(false);
        }

        Move();
        MouseMoveCamera();
    }

    private void RunToggle(bool state)
    {
        _currentMoveSpeed = state ? runSpeed : walkSpeed;
    }

    private void Move()
    {
        var h = Input.GetAxis("Horizontal");
        var v = Input.GetAxis("Vertical");

        _direction.x = h;
        _direction.z = v;
        _direction.Normalize();
        transform.position += (_direction.z * transform.forward+ _direction.x * transform.right) * (_currentMoveSpeed * Time.deltaTime) ;
    }

    private void MouseMoveCamera()
    {
        var inputXMouse = Input.GetAxis("Mouse X");

        transform.Rotate(Vector3.up * inputXMouse * rotationSpeed * Time.deltaTime, Space.Self);
    }
    
    
}