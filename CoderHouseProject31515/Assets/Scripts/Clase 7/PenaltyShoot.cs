using System;
using UnityEngine;

public class PenaltyShoot : MonoBehaviour
{

    [SerializeField] private Rigidbody ballRigidBody;
    [SerializeField] private float penaltyIntensity;

    [SerializeField] private Vector3 direction;
    [SerializeField] private Transform startPoint;
    
    
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Shoot();
        }
        
        CheckChangeDirection();
    }

   
    private void Shoot()
    {
        Debug.Log(GameManager.Instance.Score);
        var directionToShoot = new Vector3(direction.x, direction.y, ballRigidBody.transform.forward.z);
        ballRigidBody.AddForce(directionToShoot * penaltyIntensity, ForceMode.Impulse);
    }

    private void CheckChangeDirection()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            direction -= ballRigidBody.transform.right;
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            direction += ballRigidBody.transform.right;
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            direction += ballRigidBody.transform.up;
        }
        direction.Normalize();
    }
}
