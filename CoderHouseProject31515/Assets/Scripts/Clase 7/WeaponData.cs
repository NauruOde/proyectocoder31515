﻿using UnityEngine;

namespace _OctoSoft.Scripts.FPSRelated.Guns
{
    [CreateAssetMenu(fileName = "NewWeaponData", menuName = "ScriptableObjects/Weapons", order = 0)]
    public class WeaponData : ScriptableObject
    {
        [field: SerializeField] public float BulletDamage { get; private set; }
        [field: SerializeField] public int MaxBulletPerMagazine { get; private set; }
        [SerializeField] private float bulletsPerSecond;
        public float DelayBetweenBullets => 1f / bulletsPerSecond;
        [field: SerializeField] public int DefaultAmountOfMagazines { get; private set; } = 3;
        [field: SerializeField] public float Range { get; private set; }
        [field: SerializeField] public LayerMask playerLayerToAffect;
        //[field: SerializeField] public FXPlayer hitSomethingEffect;
        [field: SerializeField] public Sprite weaponSprite;
        [field: SerializeField] public float reloadTime;
    }
}