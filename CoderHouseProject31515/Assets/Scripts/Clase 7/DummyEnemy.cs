using Interface;
using UnityEngine;

public class DummyEnemy : MonoBehaviour, IDamageable
{
    [SerializeField] private float maxHealth;

    private float _currentHealth;

    private void Awake()
    {
        _currentHealth = maxHealth;
    }

    public void TakeDamage(float damage)
    {
        _currentHealth -= damage;
        
        Debug.Log($"Current health is {_currentHealth}");

        if (_currentHealth > 0)
            return;

        _currentHealth = 0;

        Destroy(gameObject);
    }

    public float MaxHealth => maxHealth;

    public void GetHeal(float healAmount)
    {
        _currentHealth += healAmount;

        if (_currentHealth > maxHealth)
        {
            _currentHealth = maxHealth;
        }
    }
}