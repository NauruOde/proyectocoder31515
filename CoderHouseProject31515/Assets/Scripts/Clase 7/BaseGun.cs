﻿using Interface;
using UnityEngine;

namespace _OctoSoft.Scripts.FPSRelated.Guns
{
    public class BaseGun : MonoBehaviour
    {
        [field: SerializeField] public WeaponData WeaponData { get; private set; }
        public GameObject ModelOwner => gameObject;

       
        private bool _isEquipped;
        protected float _currentCooldown;
        protected Camera _mainCam;
       // [SerializeField] private FXPlayer muzzleEffect;
        private Collider _collider;

        protected int _bulletsInMagazine;
        protected int _currentBullets;
        protected bool _reloading;
       


        protected RaycastHit[] hitTargets1 = new RaycastHit[1];
      
      

       



       

        protected virtual void Awake()
        {
            _isEquipped = false;
            _mainCam = Camera.main;
            _collider = GetComponent<Collider>();
            _bulletsInMagazine = WeaponData.MaxBulletPerMagazine * WeaponData.DefaultAmountOfMagazines;
            _bulletsInMagazine -= WeaponData.MaxBulletPerMagazine;
            _currentBullets += WeaponData.MaxBulletPerMagazine;
        }

        public virtual void Shoot()
        {
            if (_currentCooldown > Time.time || !_isEquipped)
                return;

            if (_currentBullets == 0 && !_reloading)
            {
                _reloading = true;
                Reload();
                _currentCooldown = Time.time + WeaponData.DelayBetweenBullets;
                return;
            }

            _currentCooldown = Time.time + WeaponData.DelayBetweenBullets;

            //muzzleEffect?.PlayEffect();

            ShootingRaycast();

           // if (!_isFPSUIManagerNotNull) return;

            _currentBullets--;

//            FPSUIManager.Instance.SetCurrentBullets(_currentBullets);
        }

        public virtual void Reload()
        {
            var desiredBullets = WeaponData.MaxBulletPerMagazine - _currentBullets;

            if (desiredBullets == 0) return;

            var bulletToLoad = (desiredBullets < _bulletsInMagazine) ? desiredBullets : _bulletsInMagazine;

            _bulletsInMagazine -= bulletToLoad;
            _currentBullets += bulletToLoad;

            _reloading = false;

            _currentCooldown += WeaponData.reloadTime;

            //if (!_isFPSUIManagerNotNull) return;

            //FPSUIManager.Instance.SetCurrentBullets(_currentBullets);
            //FPSUIManager.Instance.SetCurrentMagazine(_bulletsInMagazine);
        }

        public void GetBullets(int magazinesObtained)
        {
            _bulletsInMagazine += magazinesObtained * WeaponData.MaxBulletPerMagazine;
        }

        protected virtual void ShootingRaycast()
        {
            var transform1 = _mainCam.transform;
            var hitSomething = Physics.Raycast(transform1.position, transform1.forward, out var hitData,
                WeaponData.Range, WeaponData.playerLayerToAffect);

            if (!hitSomething)
            {
                return;
            }

            //Handle gun hit something particles

            /*
            var instantiatedParticle = Instantiate(WeaponData.hitSomethingEffect, hitData.point,
                Quaternion.LookRotation(hitData.normal));
            Destroy(instantiatedParticle.gameObject, 2f);
*/
            if (!hitData.collider.TryGetComponent(out IDamageable damageable)) return;

        
            damageable.TakeDamage(WeaponData.BulletDamage);
        }

        protected virtual void OnTriggerEnter(Collider other)
        {
            if (_isEquipped) return;

            //if (!other.TryGetComponent(out PlayerWeaponSystem weaponSystem)) return;
            //photonView.TransferOwnership(PhotonNetwork.LocalPlayer);
            // _collider.enabled = false;
           // weaponSystem.GetWeapon(this);
            //_isEquipped = true;
            //rotateObject.enabled = false;
//            photonView.RPC(nameof(SetWeaponAvailability), RpcTarget.AllViaServer, false);

            /*
            var weaponHash = new ExitGames.Client.Photon.Hashtable
            {
                {EQUIPPED_WEAPON_DATA_SEND, _isEquipped},
                {COLLIDER_STATE_DATA_SEND, _collider.enabled},
                {SPINNING_STATE_DATA_SEND, rotateObject.enabled}
            };

            PhotonNetwork.LocalPlayer.SetCustomProperties(weaponHash);*/
        }

        /*
        public void SetPlayerTeam(Team playerTeam)
        {
            _playerTeam = playerTeam;
        }

        [PunRPC]
        public void SetWeaponAvailability(bool available)
        {
            _isEquipped = !available;
            _collider.enabled = available;
            rotateObject.enabled = available;
        }
*/
        /*
        public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
        {
            if (!photonView.IsMine && targetPlayer == photonView.Owner)
            {
                Debug.Log("Updated some players stats");
                _isEquipped = (bool) changedProps[EQUIPPED_WEAPON_DATA_SEND];
                _collider.enabled = (bool) changedProps[COLLIDER_STATE_DATA_SEND];
                rotateObject.enabled = (bool) changedProps[SPINNING_STATE_DATA_SEND];
            }
        }*/

        /* 
         public void ThrowWeapon()
         {
             _isEquipped = false;
             _collider.enabled = true;
         }*/
    }
}