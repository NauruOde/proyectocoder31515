using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    [SerializeField] private string sceneToLoadName;
    public int levelIndex;
    [SerializeField] private bool loadLevel;

    public float festejo;
    public Color teamColor;

    private TeamManager _teamManager;
    
    public float Score { get; private set; }
    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
        _teamManager = GetComponentInChildren<TeamManager>();
    }
    public void ScoreGoal()
    {
        Score++;
        _teamManager.OnGoal();
    }

    public int GetScore()
    {
        return _teamManager.Score;
    }
}
