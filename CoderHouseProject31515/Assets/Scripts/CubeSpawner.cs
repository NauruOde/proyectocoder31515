using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeSpawner : MonoBehaviour
{

    [SerializeField] private GameObject cubePrefab;
    [SerializeField] private Transform spawnLocation;
   

    private void Start()
    {
        Instantiate(cubePrefab, spawnLocation.position, spawnLocation.rotation);
    }

}
