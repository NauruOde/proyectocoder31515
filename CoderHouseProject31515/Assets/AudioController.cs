using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{

    [SerializeField] private AudioSource music;
    [SerializeField] private AudioClip musicToPlay;


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            PlayMusicFromAS();
        }

        if (Input.GetKeyDown(KeyCode.K))
        {
            PlayMusicFromCLip();
        }
    }

    private void PlayMusicFromAS()
    {
        music.Play();
    }

    private void PlayMusicFromCLip()
    {
        AudioSource.PlayClipAtPoint(musicToPlay, transform.position);
    }
}
