using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class LightController : MonoBehaviour
{
    [SerializeField] private Light light;


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            ChangeLight();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            LightEnabler();
        }
    }

    private void ChangeLight()
    {
        light.intensity = 4;
        
        light.color = Color.cyan;

        light.shadows = LightShadows.Soft;

        light.shadowResolution = LightShadowResolution.VeryHigh;
    }

    private void LightEnabler()
    {
        light.enabled = !light.enabled;
    }




}
