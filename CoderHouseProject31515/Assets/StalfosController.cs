using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StalfosController : MonoBehaviour
{
    [SerializeField] private Transform playerTransform;

    [SerializeField] private float speed;

    [SerializeField] private float minimumDistance;
    [SerializeField] private float rotationSpeed;
    [SerializeField] private float moveSpeedLerp;

    private void Update()
    {
        Chase();
    }

    private void Chase()
    {
        var playerRefDistance = playerTransform.position - transform.position;

        var distanceToPlayer = playerRefDistance.magnitude;
        var dirToPlayer = playerRefDistance.normalized;
        Move(dirToPlayer, distanceToPlayer, playerRefDistance);
    }

    private void Move(Vector3 dir, float distanceToPlayer, Vector3 playerRefDistance)
    {
        var dirMultiplier = distanceToPlayer > minimumDistance ? 1 : -1;

        var newRotation = Quaternion.LookRotation(playerRefDistance);
        transform.rotation = Quaternion.Lerp(transform.rotation, newRotation, Time.deltaTime * rotationSpeed);
        var targetPos = transform.position+dir * (dirMultiplier * (Time.deltaTime * speed));
        transform.position = Vector3.Lerp(transform.position, targetPos, Time.deltaTime * moveSpeedLerp);
    }
}