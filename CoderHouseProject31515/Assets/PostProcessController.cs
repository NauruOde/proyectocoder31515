using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessController : MonoBehaviour
{
    private PostProcessVolume _postProcessVolume;
    private Vignette _vignette;

    private ReflectionProbe _probe;
    
    
    
    private void Awake()
    {
        _postProcessVolume = GetComponent<PostProcessVolume>();
        

        if (_postProcessVolume.profile.TryGetSettings(out Vignette _vignette))
        {
            _vignette.intensity.value = 1f;
        }

        if (_postProcessVolume.profile.TryGetSettings(out MotionBlur blur))
        {
          
        }
        else
        {
            Debug.Log("You don't have an auto exposure parameter");
        }
    }
    
    
}
