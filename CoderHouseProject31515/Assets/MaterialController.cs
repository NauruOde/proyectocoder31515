using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialController : MonoBehaviour
{

    
    private Renderer _renderer;

    private static readonly int _glossinessId = Shader.PropertyToID("_Glossiness");
    private static readonly int Color1 = Shader.PropertyToID("_Color");

    private void Awake()
    {
        _renderer = GetComponent<Renderer>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ChangeMat();
        }
    }

    private void ChangeMat()
    {
        _renderer.material.SetFloat(_glossinessId, 0);
        var newColor = Color.green;
        newColor.a = 0.2f;
        _renderer.material.SetColor(Color1, newColor);
    }
}
