using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private GameObject camera1, camera2;

    [SerializeField] private int maxBullets;

  

    private void Reload()
    {
        
    }
    private void Start()
    {
        SetupInitialConditions();
    }

    private void SetupInitialConditions()
    {
        camera1.SetActive(true);
        camera2.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            camera2.SetActive(!camera2.activeSelf);
            camera1.SetActive(!camera1.activeSelf);
        }
    }
}