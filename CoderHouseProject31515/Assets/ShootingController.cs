using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using JetBrains.Annotations;
using UnityEngine;

public class ShootingController : MonoBehaviour
{
    [SerializeField] private BulletController bulletPrefab;
    [SerializeField] private Transform bulletSpawnPoint;

    [SerializeField] private KeyCode shootKey;

    [SerializeField] private float rotationSpeed;

    [SerializeField] private Vector3 newRotation;

    [SerializeField] private KeyCode rotateRight, rotateLeft;
    [SerializeField] private Transform takeRotationFrom;

    [SerializeField] private Transform theSun;

    private bool _rotateRightInput;

    private void Update()
    {
        if (Input.GetKeyDown(shootKey))
        {
            Shoot();
        }

       
        RotateObject();
    }


    private void RotateObject()
    {
        Debug.Log($"Object is rotating");

        //transform.rotation = takeRotationFrom.rotation;
        //transform.forward = -takeRotationFrom.forward;

        //var rotateDior = _rotateRightInput ? 1 : -1;
        transform.Rotate(transform.up, Time.deltaTime * rotationSpeed);
       // transform.RotateAround(theSun.position, theSun.up, Time.deltaTime * rotationSpeed);
       
       transform.Rotate(Vector3.up * (Time.deltaTime * rotationSpeed), Space.World);
    }

    private void Shoot()
    {
        //
        Debug.Log($"shoot");

        var bullet = Instantiate(bulletPrefab, bulletSpawnPoint.position, Quaternion.identity);

        bullet.SetDirection(bulletSpawnPoint.forward);
    }
}