using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartPiece : MonoBehaviour
{
    [SerializeField] private float healAmount;

    
    
    

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Colisione con un objeto");

        var healthController = other.GetComponentInParent<HealthController>();
        if (healthController != null)
        {
            healthController.GetHeal(healAmount);
            Destroy(gameObject);
        }
        
    }
}