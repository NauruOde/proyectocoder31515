using System.Collections;
using System.Collections.Generic;
using Interface;
using UnityEngine;

public class HealthController : MonoBehaviour, IDamageable
{
    [SerializeField] private float maxHealth;
    private float _currentHealth = 30;

    public float MaxHealth => maxHealth;

    public void TakeDamage(float damage)
    {
        _currentHealth -= damage;

        if (_currentHealth > 0)
            return;
        _currentHealth = 0;
        Die();
    }

    private void Die()
    {
        Destroy(gameObject);
    }

    public void GetHeal(float healAmount)
    {
        _currentHealth += healAmount;
        
        if (_currentHealth < maxHealth) return;
        _currentHealth = maxHealth;
    }
}