using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinkMovementController : MonoBehaviour
{
    [SerializeField] private float moveSpeed;
    [SerializeField] private float rotationSpeed;

    private Collider _myCollider;


    private void Awake()
    {
        _myCollider = GetComponentInChildren<Collider>();
    }

    public void RecieveInputs(float inputX, float inputY)
    {
        Move(inputY);
        Rotate(inputX);
    }

    private void Move(float input)
    {
        transform.position += -transform.right * (moveSpeed * input * Time.deltaTime);
    }

    private void Rotate(float input)
    {
        transform.Rotate(Vector3.up * (Time.deltaTime * input * rotationSpeed), Space.Self);
    }
}